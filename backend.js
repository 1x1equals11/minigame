var sixLetterWord = ["UNSURE","POWDER","PAINED"];
var painedMatchWords = ["PAINED","PANED","PINED","AIDE","APED","DEAN","DENI","DINE","IDEA","NAPE","NEAP","NIDE","NIPA","PADI","PAID","PAIN","PANE","PEAN","PEIN","PEND","PIAN","PIED","PINA","PINE","AID","AIN","AND","ANE","ANI","APE","DAN","DAP","DEI","DEN","DIE","DIN","DIP","END","NAE","NAP","NIP","PAD","PAN","PEA","PED","PEN","PIA","PIE","PIN"];
var unsureMatchWords = ["UNSURE","NURSE","RUNES","ERNS","RUES","RUNE","RUSE","SUER","SURE","URNS","URUS","USER","ENS","ERN","ERS","NUS","RES","RUE","RUN","SEN","SER","SUE","SUN","UNS","URN","USE"];
var powderMatchWords = ["POWDER","DOPER","DOWER","PORED","POWER","ROPED","ROWED","DOER","DOPE","DORE","DORP","DREW","DROP","OPED","OWED","PORE","PROD","PROW","REDO","REPO","RODE","ROPE","WORD","WORE","DEW","DOE","DOR","DOW","ODE","OPE","ORE","OWE","PED","PER","PEW","POD","POW","PRO","RED","REP","ROD","ROE","ROW","WED","WOE","WOP"];
var wordArrayCounter = 0;
var inputBoxCounter = 50;
var liCounter=56;
var timeLeft = 240;
var counter = 0;
var remainingWords;
var interval;
var currentWord;
var randomStr;
var answer="";
var subarray=[];
var emptyChars=[];
var isEqual;

function initialize(){
    clearWordList();
  document.getElementById('startButton').style.display = "none";
  document.getElementById('wordsArea').style.display = "block";
  document.getElementById('inputBox').style.display = "block";
  document.getElementById('mixedWords').style.display = "block";
  document.getElementById('buttonList').style.display = "block";
  document.getElementById('timer').style.display = "block";
  document.getElementById('gameOverBox').style.display = "none";
  document.getElementById('nextLevel').style.display = "none";
  if(sixLetterWord[wordArrayCounter] == 'PAINED'){
    setMatchWordsArray(painedMatchWords);
  }else if (sixLetterWord[wordArrayCounter] == 'UNSURE') {
    setMatchWordsArray(unsureMatchWords);
  }else{
    setMatchWordsArray(powderMatchWords);
  }
  console.log(subarray);
  generateWord();
  appendToMixedWords();
  appendToWordList();
  clearInputBox();
  reInitializeTimer();
  runTimer();
}

function button1(){
  appendToInputBox(document.getElementById('button1').textContent);
  document.getElementById('button1').disabled = true;
  document.getElementById('button1').style.opacity = .5;
  disableScrambleButton();
}
function button2(){
  appendToInputBox(document.getElementById('button2').textContent);
  document.getElementById('button2').disabled = true;
  document.getElementById('button2').style.opacity = .5;
  disableScrambleButton();
}
function button3(){
  appendToInputBox(document.getElementById('button3').textContent);
  document.getElementById('button3').disabled = true;
  document.getElementById('button3').style.opacity = .5;
  disableScrambleButton();
}
function button4(){
  appendToInputBox(document.getElementById('button4').textContent);
  document.getElementById('button4').disabled = true;
  document.getElementById('button4').style.opacity = .5;
  disableScrambleButton();
}
function button5(){
  appendToInputBox(document.getElementById('button5').textContent);
  document.getElementById('button5').disabled = true;
  document.getElementById('button5').style.opacity = .5;
  disableScrambleButton();
}
function button6(){
  appendToInputBox(document.getElementById('button6').textContent);
  document.getElementById('button6').disabled = true;
  document.getElementById('button6').style.opacity = .5;
  disableScrambleButton();
}

function checkInput(){

  isEqual = false;
  for (var i = 0; i < subarray.length; i++){
    if(answer == subarray[i]){
      document.getElementsByTagName('li')[i].textContent = answer;
      isEqual = true;
      remainingWords--;
    }
  }
  clearInputBox();
  if(remainingWords == 0){
    if(wordArrayCounter == sixLetterWord.length){
      gameOver();
    }else{
      document.getElementById('nextLevel').style.display = "block";
      reInitializeTimer();
      disableButtons();
    }
  }
  console.log(wordArrayCounter);
  document.getElementById('twist').disabled = false;
}
function clearWordList(){
  for (var i = 0; i < subarray.length; i++) {
    document.getElementsByTagName('li')[i].textContent = " ";
  }
}

function appendToWordList(){
  for(var x=0; x<subarray.length; x++){ //init emptychars array
    emptyChars[x] = subarray[x];
  }

  for(var x=0; x<emptyChars.length; x++){//convert subarray to empty chars
    for(var y=0; y<emptyChars[x].length; y++){
      emptyChars[x] = emptyChars[x].replace(emptyChars[x][y],"*");
    }
  }

  for(var x=0; x<emptyChars.length; x++){//print to page
    document.getElementsByTagName('li')[x].textContent = emptyChars[x];
  }
}

function appendToInputBox(letter){
  if(inputBoxCounter < 56 ){
      document.getElementsByTagName('li')[inputBoxCounter].innerHTML = letter;
      inputBoxCounter++;
      this.answer +=letter;
  }
  console.log(this.answer);
}

function generateWord(){
  currentWord = sixLetterWord[wordArrayCounter];
  wordArrayCounter = wordArrayCounter + 1;
}

function setMatchWordsArray(arr){
  subarray = arr;
  remainingWords = subarray.length;
}

function appendToMixedWords(){
  randomStr = currentWord.split('').sort(randomNum);
  liCounter=56;
  function randomNum() {
    var x = Math.random();
    if(x>.5){
      return -1
    }
    else{
      return 1;
    }
  }
  for(var x=0; x<randomStr.length; x++){
    document.getElementsByTagName('li')[liCounter].innerHTML = randomStr[x];
    liCounter++;
  }
//  document.getElementById('mixedWords').innerHTML = randomStr;
}

function clearInputBox(){
  for (var i = 50; i < 56; i++) {
    document.getElementsByTagName('li')[i].innerHTML = " ";
  }
  inputBoxCounter = 50;
  answer = "";
  document.getElementById('button1').disabled = false;
  document.getElementById('button2').disabled = false;
  document.getElementById('button3').disabled = false;
  document.getElementById('button4').disabled = false;
  document.getElementById('button5').disabled = false;
  document.getElementById('button6').disabled = false;
  document.getElementById('twist').disabled = false;
  document.getElementById('enter').disabled = false;
  document.getElementById('clear').disabled = false;
  document.getElementById('button1').style.opacity = 1;
  document.getElementById('button2').style.opacity = 1;
  document.getElementById('button3').style.opacity = 1;
  document.getElementById('button4').style.opacity = 1;
  document.getElementById('button5').style.opacity = 1;
  document.getElementById('button6').style.opacity = 1;
}

/*Game Over*/
function gameOver(){
    wordArrayCounter = 0;
    inputBoxCounter = 50;
    interval;
    currentWord;
    randomStr;
    answer="";
    emptyChars=[];
    liCounter = 56;
    reInitializeTimer();
    disableButtons();
    disableScrambleButton();
    clearWordList();
    document.getElementById('gameOverBox').style.display = "block";
    //initialize();
}

/*Timer*/
function runTimer(){
  function convertSeconds(sec){
    var minute = Math.floor(sec/60);
    var seconds = sec%60;
    return minute+':'+seconds;
  }
  function timer(){
    counter++;
    document.getElementById('time').textContent = 'TIME: '+(convertSeconds(timeLeft-counter));
    if(counter == 240){
      gameOver();
    }
  }
  interval = setInterval(timer,1000);
}

function reInitializeTimer(){
  counter=0;
  timeLeft-60;
  clearInterval(interval);
}

function disableButtons(){
  document.getElementById('button1').disabled = true;
  document.getElementById('button2').disabled = true;
  document.getElementById('button3').disabled = true;
  document.getElementById('button4').disabled = true;
  document.getElementById('button5').disabled = true;
  document.getElementById('button6').disabled = true;
  document.getElementById('enter').disabled = true;
  document.getElementById('clear').disabled = true;
}

function disableScrambleButton(){
  document.getElementById('twist').disabled = true;
}


/*Global Conditions*/
